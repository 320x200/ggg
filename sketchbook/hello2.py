#!/usr/bin/env python3
#

from bottle import Bottle ,redirect
from bottle.ext.mongo import MongoPlugin

from bson.json_util import dumps


app = Bottle()
plugin = MongoPlugin(uri="mongodb://127.0.0.1", db="ggg", json_mongo=True)
app.install(plugin)

@app.route('/', method='GET')
def index(mongodb):
    return dumps(mongodb['ggg'].find())

app.run(host='localhost', port=8080, debug=True, reloader=True)