#!/usr/bin/env python3
#

import os
from bottle import route, run, template, get, post, request, static_file


def check_add_item(title, platform, upload):
	if title != "" and platform != "" and upload != "" : return True 


@route('/static/<filepath:path>')
def server_static(filepath):
	return static_file(filepath, root='./static')

@route('/item/<platform>/<title>')
def display_item(platform='unknown', title='unknown'):
	return template('Viewing {{title}} ({{platform}}) <img src="/static/img/download.gif" />', platform=platform, title=title)


@get('/add')
def add_item():
	return '''
		<form action="/add" method="post" enctype="multipart/form-data">
			Title: <input name="title" type="text" />
			Platform: <input name="platform" type="text" />
			Cover: <input name="upload" type="file" />
			<input value="Add" type="submit" />
		</form>
	'''

@post('/add')
def do_add_item():
	title = request.forms.get('title')
	print(title)
	platform = request.forms.get('platform')
	upload = request.files.get('upload')
	name, ext = os.path.splitext(upload.filename)
	if check_add_item(title, platform, upload):
		upload.save("static/img/"+platform+"-"+title+ext)
		return '''
			<p>Thanks for adding '''+title+''' ('''+platform+''').</p>
			<p>You can view the item <a href="/item/'''+platform+'''/'''+title+'''">here</a>.'''
	else:
		return "Title and Platform forms are mandatory."

run(host='localhost', port=8080, debug=True, reloader=True)