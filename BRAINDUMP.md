Wed 21 Oct 2015
---------------

- Implemented POST /items
- I probably need to look into JSON Schema now


Tue 20 Oct 2015
---------------

- Found out that there is a way to send/edit different HTTP requests and methods from Firefox in the dev tools (Network tab), otherwise curl works fine too:
```
curl [-i] -X POST -H "application/json" -d '{"key":"val"}' URL
```
- POST is working, data is received but can't get the right way to serialise it.


Fri 16 Oct 2015
---------------

- I was trying to figure out how to get the result from dumps into pretty printed json, but that was starting to be a waste of time, quick fix: https://addons.mozilla.org/en-us/firefox/addon/jsonview :)
- implemented GET /items/:id
- ObjectId() converts string from request URL to ObjectId
- InvalidId is raised when ObjectId data is invalid


Thu 15 Oct 2015
---------------

- decided to separate everything in 2 collections: items, auctions
```
mongoimport --db ggg --collection items --drop --file sample_items.json --jsonArray
```
- implemented GET /items
- dumps from json_utils takes care of all bson<->json conversions
- started to implement GET /items/:id


misc
----

Need to implement errors properly and maybe some caching techniques
see www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api




