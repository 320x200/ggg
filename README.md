What?
=====

GGG is a web application and API to manage and keep track of Yahoo! auctions.


API (working)
=============

```
GET		/items					- Retrieves the list of items
GET		/items/666				- Retrieves item 666
POST	/items					- Creates a new item
```


API (planned)
=============

Nouns and Verbs
---------------

```
PUT		/items/666				- Updates item 666
PATCH	/items/666				- Partially updates item 666
DELETE	/items/666				- Deletes item 666

GET		/items/666/auctions		- Retrieves a list of sales for item 666
GET		/items/666/auctions/3	- Retrieves auction 3 for item 666
POST	/items/666/auctions		- Creates a new auction for item 666
PUT		/items/666/auctions/3	- Updates auction 3 for item 666
PATCH	/items/666/auctions/3	- Partially updates auction 3 for item 666
DELETE	/items/666/auctions/3	- Deletes auction 3 for item 666
```


Filters
-------

```
GET	/items						- Get the list of all items
GET	/items?type=pce				- Get the list of all PCE related items
GET	/items?type=lucky			- Get one random item of any type
GET	/items?sort=latin			- Get all items sorted by latin alphabet
GET	/items?type=sfc&sort=kana	- Get all SFC items sorted by kana
```

Aliases for Common Queries
--------------------------

```
GET	/auctions				    - Retrieves all the auctions?
```