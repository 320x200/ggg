#!/usr/bin/env python3
#

from bson import Binary, Code
from bson.json_util import dumps, loads
from bson.objectid import ObjectId
from bottle import route, run, request, abort, response
from pymongo import MongoClient
from pymongo.errors import InvalidId


# Parameters MongoDB
client = MongoClient('localhost', 27017)
db = client.ggg
items = db.items



@route('/items', method='GET')
def get_all_items():
	all_items = items.find()	
	if not all_items:
		abort(404, 'No items found')
	response.content_type = 'application/json; charset=UTF-8'
	return dumps(all_items)


@route('/items', method='POST')
def post_item():
	data = request.json
	if not data:
		abort(400, 'No data received')
	items.insert(data)


@route('/items/:id', method='GET')
def get_item(id):
	try:
		item = items.find_one({"_id": ObjectId(id)})
		if not item:
			abort(404, 'No item with id %s' % id)
		response.content_type = 'application/json; charset=UTF-8'
		return dumps(item)
	except(InvalidId):
		abort(500, 'Invalid id %s' % id)


run(host='localhost', port=8080, debug=True, reloader=True)